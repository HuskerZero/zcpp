#ifndef IMOUSEBRAIN_H
#define IMOUSEBRAIN_H
#include <iostream>
#include "Direction.h"
#include "Cell.h"

enum class Action { NONE, MOVING, TURNING };

class IMouseBrain
{
public:
    virtual std::pair<Action, Direction> decideNextMove(std::shared_ptr<Cell> cell, Direction currentDirection, Action currentAction) = 0;

protected:
    Direction oppositeDirection(Direction dir)
    {
        switch(dir)
        {
        case Direction::UP:
            return Direction::DOWN;
        case Direction::RIGHT:
            return Direction::LEFT;
        case Direction::DOWN:
            return Direction::UP;
        case Direction::LEFT:
            return Direction::RIGHT;
        }
    }
};

#endif // IMOUSEBRAIN_H
