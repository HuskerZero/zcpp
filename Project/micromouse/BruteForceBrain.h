#ifndef BRUTEFORCEBRAIN_H
#define BRUTEFORCEBRAIN_H

#include "IMouseBrain.h"

class BruteForceBrain : public IMouseBrain
{
public:
    BruteForceBrain();

    // IMouseBrain interface
public:
    virtual std::pair<Action, Direction> decideNextMove(std::shared_ptr<Cell> cell, Direction currentDirection, Action currentAction) override;
};

#endif // BRUTEFORCEBRAIN_H
