#include "Cell.h"
#include <algorithm>
#include <iostream>
#include <QJsonArray>

int Cell::nextId = 0;

Cell::Cell(int x, int y, Type type) : x(x), y(y), cellType(type), visited(false)
{
    id = nextId;
    ++nextId;
    connections.reserve(4);
}

std::vector<Direction> Cell::getConnections() const
{
    return connections;
}

void Cell::addConnection(Direction direction)
{
    auto connection = std::find(connections.begin(), connections.end(), direction);
    if (connection == connections.end())
    {
        connections.push_back(direction);
    }
}

void Cell::deleteConnection(Direction direction)
{
    auto connection = std::find(connections.begin(), connections.end(), direction);
    if (connection != connections.end())
    {
        connections.erase(connection);
    }
}

bool Cell::hasConnection(Direction direction)
{
    auto connection = std::find(connections.begin(), connections.end(), direction);
    if (connection != connections.end())
    {
        return true;
    }
    return false;
}

int Cell::getId() const
{
    return id;
}

int Cell::getX() const
{
    return x;
}

int Cell::getY() const
{
    return y;
}

void Cell::setVisited(bool v)
{
    visited = v;
}

bool Cell::isVisited()
{
    return visited;
}

Cell::Type Cell::getCellType()
{
    return cellType;
}

std::shared_ptr<QJsonObject> Cell::toJson() const
{
    auto json = std::make_shared<QJsonObject>();
    json->insert("id", id);
    json->insert("x", x);
    json->insert("y", y);

    QJsonArray jsonConnections;
    for(const auto &connection : connections)
    {
        jsonConnections.append(static_cast<int>(connection));
    }
    json->insert("connections", jsonConnections);
    return json;
}

