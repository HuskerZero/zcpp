#ifndef CELL_H
#define CELL_H
#include <vector>
#include <QJsonObject>
#include "Direction.h"

class Cell
{
public:
    enum Type { NORMAL, START, FINISH };
    Cell(int x, int y, Type type);
    std::vector<Direction> getConnections() const;
    void addConnection(Direction direction);
    void deleteConnection(Direction direction);
    bool hasConnection(Direction direction);
    int getId() const;
    int getX() const;
    int getY() const;
    void setVisited(bool v);
    bool isVisited();
    Type getCellType();
    std::shared_ptr<QJsonObject> toJson() const;
private:
    int id;
    int x;
    int y;
    Type cellType;
    bool visited;
    static int nextId;
    std::vector<Direction> connections;
};

#endif // CELL_H
