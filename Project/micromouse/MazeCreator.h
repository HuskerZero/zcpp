#ifndef MAZECREATOR_H
#define MAZECREATOR_H

#include <memory>
#include <vector>
#include "Maze.h"
#include "IMazeGenerator.h"

class MazeCreator
{
public:
    MazeCreator();
    std::shared_ptr<Maze> generateMaze(int size, std::string algorithm);
    std::vector<std::string> getAvailableAlgorithms();

private:
    std::shared_ptr<IMazeGenerator> createGenerator(std::string name);

};

#endif // MAZECREATOR_H
