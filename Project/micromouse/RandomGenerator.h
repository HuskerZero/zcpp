#ifndef RANDOMDIRECTION_H
#define RANDOMDIRECTION_H
#include <random>
#include <chrono>
#include <vector>
#include "Direction.h"

class RandomGenerator
{
    private:
    std::default_random_engine engine;

    public:
    RandomGenerator()
    {
        engine.seed(std::chrono::steady_clock::now().time_since_epoch().count());
    }

    Direction generateRandom(std::vector<Direction> directions)
    {
        std::uniform_real_distribution<> dist(0, directions.size());
        return directions[dist(engine)];
    }

    int generateRandomIndex(int maxVal)
    {
        std::uniform_real_distribution<> dist(0, maxVal);
        return int(dist(engine));
    }
};

#endif // RANDOMDIRECTION_H
