#ifndef MOUSECREATOR_H
#define MOUSECREATOR_H

#include <memory>
#include <vector>
#include "Maze.h"
#include "IMouseBrain.h"

class MouseCreator
{
public:
    MouseCreator();
    std::vector<std::string> getAvailableAlgorithms();
    std::shared_ptr<IMouseBrain> createMouse(std::string name);
};

#endif // MOUSECREATOR_H
