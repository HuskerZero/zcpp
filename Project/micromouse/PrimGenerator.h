#ifndef PRIMGENERATOR_H
#define PRIMGENERATOR_H

#include "IMazeGenerator.h"

class PrimGenerator : public IMazeGenerator
{
public:
    PrimGenerator(); 
    // IMazeGenerator interface
    virtual std::shared_ptr<Maze> generateMaze(int size) override;

private:
    void createConnection(std::shared_ptr<Cell> cellFrom, std::shared_ptr<Cell> cellTo);
    void appendCellWalls(std::shared_ptr<Cell> cell, std::shared_ptr<Maze> maze, std::vector<std::pair<std::shared_ptr<Cell>, std::shared_ptr<Cell>>> &walls);
    Direction determineDirection(std::shared_ptr<Cell> cellFrom, std::shared_ptr<Cell> cellTo);
};

#endif // PRIMGENERATOR_H
