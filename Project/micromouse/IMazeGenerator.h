#ifndef IMAZEGENERATOR_H
#define IMAZEGENERATOR_H

#include <memory>
#include "Maze.h"

class IMazeGenerator
{
public:
    virtual std::shared_ptr<Maze> generateMaze(int size) = 0;
};

#endif // IMAZEGENERATOR_H
