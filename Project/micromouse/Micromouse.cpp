#include <QGraphicsScene>
#include <QPainter>
#include <QRandomGenerator>
#include <QStyleOption>
#include <QtMath>
#include <QDebug>
#include "Micromouse.h"
#include "RandomGenerator.h"

Micromouse::Micromouse(double size, int speed, int turnSpeed, std::shared_ptr<Maze> maze, std::shared_ptr<IMouseBrain> brain) :
    turnAngles{{Direction::UP, 0.}, {Direction::RIGHT, 90.}, {Direction::DOWN, 180.}, {Direction::LEFT, 270.}},
    color(QRandomGenerator::global()->bounded(256),
          QRandomGenerator::global()->bounded(256),
          QRandomGenerator::global()->bounded(256)),
    mouseSize(size),
    maze(maze),
    angleSpeed(turnSpeed),
    forwardSpeed(speed),
    mouseBrain(brain)
{
    setTransformOriginPoint((mouseSize + mouseSize/3.)/2., (mouseSize + mouseSize/3.)/2.);
    setPos(mouseSize/3., mouseSize/3.);
    setRotation(rotationAngle);
    qDebug() << "Creating mouse at x=" << positionX << "|y=" << positionY << "|size=" << mouseSize << "\n";
}

QRectF Micromouse::boundingRect() const
{
    return QRectF(0, 0, mouseSize + mouseSize/3., mouseSize + mouseSize/3.);
}

void Micromouse::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    qDebug() << "Painting mouse at x=" << positionX << "|y=" << positionY << "|size=" << mouseSize << "\n";
    // Body
    painter->setBrush(color);
    painter->drawEllipse(0 + mouseSize/6., 0, mouseSize, mouseSize + mouseSize/3.);

    // Eyes
    painter->setBrush(Qt::white);
    painter->drawEllipse(mouseSize/6.- mouseSize/8. + mouseSize/4., mouseSize/4., mouseSize/4., mouseSize/4.);
    painter->drawEllipse(mouseSize/6. + mouseSize - mouseSize/8. - mouseSize/4., mouseSize/4., mouseSize/4., mouseSize/4.);
}

void Micromouse::advance(int step)
{
    qDebug() << "Advancing\n";
    if (!step)
        return;

    switch(currentAction)
    {
    case Action::NONE:
        decideNextMove();
        break;
    case Action::MOVING:
        move(direction);
        break;
    case Action::TURNING:
        turn();
        break;
    }

    qDebug() << pos();
}

void Micromouse::turn()
{
    for(int i = 0; i < angleSpeed; i++)
    {
        if(turnByStep()) break;
    }
}

bool Micromouse::turnByStep()
{
    if(clockwiseTurn())
    {
        rotationAngle++;
        rotationAngle = (rotationAngle == 360) ? 0 : rotationAngle;
    }
    else
    {
        rotationAngle--;
        rotationAngle = (rotationAngle == -1) ? 359 : rotationAngle;
    }

    setRotation(rotationAngle);
    if(rotationAngle == turnAngles.at(direction))
    {
        currentAction = Action::MOVING;
        lastDirection = direction;
        return true;
    }
    else return false;
}

void Micromouse::move(Direction dir)
{   
    for(int i = 0; i < forwardSpeed; i++)
    {
        if(moveByStep(dir)) break;
    }
}

bool Micromouse::moveByStep(Direction dir)
{
    switch(dir)
    {
    case Direction::UP:
        moveBy(0, -1);
        break;
    case Direction::RIGHT:
        moveBy(1, 0);
        break;
    case Direction::DOWN:
        moveBy(0, 1);
        break;
    case Direction::LEFT:
        moveBy(-1, 0);
        break;
    }

    if(isStopRequired())
    {
        currentAction = Action::NONE;
        return true;
    }
    else return false;
}

bool Micromouse::clockwiseTurn()
{
    bool clockwise = true;
    switch(lastDirection)
    {
    case Direction::UP:
        if(direction == Direction::RIGHT) clockwise = true;
        else if(direction == Direction::LEFT) clockwise =  false;
        break;
    case Direction::RIGHT:
        if(direction == Direction::UP) clockwise = false;
        else if(direction == Direction::DOWN) clockwise =  true;
        break;
    case Direction::DOWN:
        if(direction == Direction::RIGHT) clockwise = false;
        else if(direction == Direction::LEFT) clockwise =  true;
        break;
    case Direction::LEFT:
        if(direction == Direction::UP) clockwise = true;
        else if(direction == Direction::DOWN) clockwise =  false;
        break;
    }
    return clockwise;
}

bool Micromouse::isWallInFront()
{
    auto mazeCellIndex = positionToCellCoordinates();
    qDebug() << "I'm in cell: x=" << mazeCellIndex.first << " y=" << mazeCellIndex.second << "\n";
    auto cell = maze->getCell(mazeCellIndex.first, mazeCellIndex.second);
    if (!cell->hasConnection(direction))
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Micromouse::decideNextMove()
{
    qDebug() << "Deciding what to do next\n";
    lastDirection = direction;

    auto mazeCellIndex = positionToCellCoordinates();
    lastDecisionPoint = mazeCellIndex;
    auto cell = maze->getCell(mazeCellIndex.first, mazeCellIndex.second);

    RandomGenerator randDir;

    auto nextMove = mouseBrain->decideNextMove(cell, direction, currentAction);
    currentAction = nextMove.first;
    direction = nextMove.second;

    if(cell->getCellType() == Cell::FINISH)
    {
        currentAction = Action::NONE;
        emit signalFinishReached();
    }
}

bool Micromouse::isInCellCenter()
{
    auto position = pos();
    //map position to mouse center
    double x = position.x() + (boundingRect().width() / 2.);
    double y = position.y() + (boundingRect().height() / 2.);
    double cellSize = maze->getRealCellSize();
    double nearestCellCenterX = (std::floor(x / cellSize) * cellSize) + (cellSize / 2.);
    double nearestCellCenterY = (std::floor(y / cellSize) * cellSize) + (cellSize / 2.);
    double tolerance = cellSize / 10.;
    qDebug() << "Real position: x=" << x << " y=" << y << "|Nearest cell center: x=" << nearestCellCenterX << " y=" << nearestCellCenterY << "|Tolerance: " << tolerance << "\n";
    if(x > (nearestCellCenterX - tolerance) && x < (nearestCellCenterX + tolerance)
            && y > (nearestCellCenterY - tolerance) && y < (nearestCellCenterY + tolerance))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Micromouse::isCrossroads()
{
    auto mazeCellIndex = positionToCellCoordinates();
    auto cell = maze->getCell(mazeCellIndex.first, mazeCellIndex.second);
    if (cell->getConnections().size() > 2)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Micromouse::isStopRequired()
{
    if(isInCellCenter() && isOnLastDecisionPoint())
    {
        return false;
    }
    else if(isInCellCenter() && isWallInFront())
    {
        qDebug() << "Warning! Wall in front.\n";
        return true;
    }
    else if(isInCellCenter() && isCrossroads())
    {
        qDebug() << "New directions available\n";
        return true;
    }
    else return false;
}

bool Micromouse::isOnLastDecisionPoint()
{
    auto currentPoint = positionToCellCoordinates();
    if(lastDecisionPoint.first == currentPoint.first && lastDecisionPoint.second == currentPoint.second)
    {
        return true;
    }
    else
    {
        return false;
    }
}

std::pair<int, int> Micromouse::positionToCellCoordinates()
{
    auto position = pos();
    //map position to mouse center
    double x = position.x() + (boundingRect().width() / 2.);
    double y = position.y() + (boundingRect().height() / 2.);
    double cellSize = maze->getRealCellSize();
    int nearestCellCenterX = std::floor(x / cellSize);
    int nearestCellCenterY = std::floor(y / cellSize);
    return { nearestCellCenterX, nearestCellCenterY };
}
