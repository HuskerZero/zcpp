QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    BruteForceBrain.cpp \
    Cell.cpp \
    FileManager.cpp \
    Maze.cpp \
    MazeCreator.cpp \
    Micromouse.cpp \
    MouseCreator.cpp \
    PrimGenerator.cpp \
    RecursiveBacktrackerGenerator.cpp \
    SimulationController.cpp \
    WallFollowerBrain.cpp \
    main.cpp \
    MainWindow.cpp

HEADERS += \
    BruteForceBrain.h \
    Cell.h \
    Direction.h \
    FileManager.h \
    IMazeGenerator.h \
    IMouseBrain.h \
    MainWindow.h \
    Maze.h \
    MazeCreator.h \
    Micromouse.h \
    MouseCreator.h \
    PrimGenerator.h \
    RandomGenerator.h \
    RecursiveBacktrackerGenerator.h \
    SimulationController.h \
    WallFollowerBrain.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
