#include "BruteForceBrain.h"
#include "RandomGenerator.h"

BruteForceBrain::BruteForceBrain()
{

}


std::pair<Action, Direction> BruteForceBrain::decideNextMove(std::shared_ptr<Cell> cell, Direction currentDirection, Action currentAction)
{
    RandomGenerator randDir;
    Direction lastDirection = currentDirection;

    if(cell->getConnections().size() > 1)
    {
        Direction oppositeDir = oppositeDirection(currentDirection);
        Direction newDirection = oppositeDir;
        while(newDirection == oppositeDir)
        {
            newDirection = randDir.generateRandom(cell->getConnections());
        }
        currentDirection = newDirection;
    }
    else
    {
        currentDirection = cell->getConnections()[0];
    }

    if(lastDirection == currentDirection)
    {
        currentAction = Action::MOVING;
    }
    else
    {
        currentAction = Action::TURNING;
    }
    return {currentAction, currentDirection};
}
