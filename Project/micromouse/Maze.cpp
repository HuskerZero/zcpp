#include "Maze.h"
#include <algorithm>
#include <QJsonArray>

Maze::Maze(int size)
{
    cells = generateEmptyMaze(size);
}

Maze::Maze(const QJsonObject &obj)
{
    int size = obj["size"].toInt();
    QJsonArray jsonCells = obj["cells"].toArray();

    cells = generateEmptyMaze(size);

    int numberOfCells = size * size;

    for(int i = 0; i < numberOfCells; i++)
    {
        QJsonObject jsonCell = jsonCells[i].toObject();
        int x = jsonCell["x"].toInt();
        int y = jsonCell["y"].toInt();
        QJsonArray jsonConnections = jsonCell["connections"].toArray();
        for(const auto &jsonConnection : jsonConnections)
        {
            int tmp_connection = jsonConnection.toInt();
            cells[x][y]->addConnection(static_cast<Direction>(tmp_connection));
        }
    }
}

int Maze::getSize() const
{
    return cells.size();
}

void Maze::setRealCellSize(double size)
{
    cellSize = size;
}

double Maze::getRealCellSize()
{
    return cellSize;
}

std::shared_ptr<Cell> Maze::getCell(int x, int y)
{
    if((x >= 0 && x < this->getSize()) &&
       (y >= 0 && y < this->getSize()))
    {
        return cells[x][y];
    }
    else
        return nullptr;
}

std::shared_ptr<Cell> Maze::getCell(int id)
{
    for (int i = 0; i < cells.size(); i++)
    {
        auto foundCell = std::find_if(cells[i].begin(), cells[i].end(), [&id](const std::shared_ptr<Cell> cell) { return cell->getId() == id; });
        if (foundCell != cells[i].end())
        {
            return cells[foundCell->get()->getX()][foundCell->get()->getY()];
        }
    }
    return nullptr;
}

std::shared_ptr<Cell> Maze::getNeighbourCell(std::shared_ptr<Cell> cell, Direction direction)
{
    switch(direction)
    {
    case Direction::UP:
        return getCell(cell->getX(), cell->getY() - 1);
    case Direction::DOWN:
        return getCell(cell->getX(), cell->getY() + 1);
    case Direction::LEFT:
        return getCell(cell->getX() - 1, cell->getY());
    case Direction::RIGHT:
        return getCell(cell->getX() + 1, cell->getY());
    }
}

std::shared_ptr<QJsonObject> Maze::toJson() const
{
    auto json = std::make_shared<QJsonObject>();
    int size = this->getSize();
    json->insert("size", size);

    QJsonArray jsonCells;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            jsonCells.append(*cells[i][j]->toJson().get());
        }
    }
    json->insert("cells", jsonCells);
    return json;
}

std::vector<std::vector<std::shared_ptr<Cell>>> Maze::generateEmptyMaze(int size)
{
    std::vector<std::vector<std::shared_ptr<Cell>>> tmp_cells(size, std::vector<std::shared_ptr<Cell>>());
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if(i == 0 && j == 0) tmp_cells[i].push_back(std::make_shared<Cell>(i, j, Cell::START));
            else if(i == (size - 1) && j == (size - 1)) tmp_cells[i].push_back(std::make_shared<Cell>(i, j, Cell::FINISH));
            else tmp_cells[i].push_back(std::make_shared<Cell>(i, j, Cell::NORMAL));
        }
    }
    return tmp_cells;
}
