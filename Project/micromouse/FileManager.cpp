#include "FileManager.h"
#include <QFileDialog>

void FileManager::saveFile(QJsonDocument &doc)
{
    QString fileName = QFileDialog::getSaveFileName(nullptr,
            "Save Maze", "",
            "Maze (*.json);;All Files (*)");
    QFile saveFile(fileName);
    if (!saveFile.open(QIODevice::WriteOnly))
    {
        qWarning("Couldn't open save file.");
        return;
    }

    saveFile.write(doc.toJson());
}

QJsonDocument FileManager::loadFile()
{
    QString fileName = QFileDialog::getOpenFileName(nullptr,
            "Load Maze", "",
            "Maze (*.json);;All Files (*)");
    QFile loadFile(fileName);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return QJsonDocument();
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    return loadDoc;
}
