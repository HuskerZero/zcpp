#include "PrimGenerator.h"
#include "RandomGenerator.h"

PrimGenerator::PrimGenerator()
{

}

std::shared_ptr<Maze> PrimGenerator::generateMaze(int size)
{
    auto maze = std::make_shared<Maze>(size);

    RandomGenerator rand;
    std::vector<std::pair<std::shared_ptr<Cell>, std::shared_ptr<Cell>>> walls;
    auto cell = maze->getCell(0, 0);
    cell->setVisited(true);
    appendCellWalls(cell, maze, walls);

    while (!walls.empty())
    {
        int idx = rand.generateRandomIndex(walls.size() -1);
        auto wall = walls[idx];

        if(wall.first->isVisited() != wall.second->isVisited())
        {
            createConnection(wall.first, wall.second);

            auto unvisitedCell = (!wall.first->isVisited()) ? wall.first : wall.second;
            unvisitedCell->setVisited(true);
            appendCellWalls(unvisitedCell, maze, walls);
        }
        walls.erase(walls.begin() + idx);
    }
    return maze;
}

void PrimGenerator::createConnection(std::shared_ptr<Cell> cellFrom, std::shared_ptr<Cell> cellTo)
{
    Direction direction = determineDirection(cellFrom, cellTo);
    cellFrom->addConnection(direction);
    switch(direction)
    {
    case Direction::UP: cellTo->addConnection(Direction::DOWN); break;
    case Direction::DOWN: cellTo->addConnection(Direction::UP); break;
    case Direction::LEFT: cellTo->addConnection(Direction::RIGHT); break;
    case Direction::RIGHT: cellTo->addConnection(Direction::LEFT); break;
    }
}

void PrimGenerator::appendCellWalls(std::shared_ptr<Cell> cell, std::shared_ptr<Maze> maze, std::vector<std::pair<std::shared_ptr<Cell>, std::shared_ptr<Cell>>> &walls)
{
    auto upCell = maze->getNeighbourCell(cell, Direction::UP);
    auto downCell = maze->getNeighbourCell(cell, Direction::DOWN);
    auto leftCell = maze->getNeighbourCell(cell, Direction::LEFT);
    auto rightCell = maze->getNeighbourCell(cell, Direction::RIGHT);

    if (upCell && !upCell->hasConnection(Direction::DOWN)) walls.push_back({cell, upCell});
    if (downCell && !downCell->hasConnection(Direction::UP)) walls.push_back({cell, downCell});
    if (leftCell && !leftCell->hasConnection(Direction::RIGHT)) walls.push_back({cell, leftCell});
    if (rightCell && !rightCell->hasConnection(Direction::LEFT)) walls.push_back({cell, rightCell});
}

Direction PrimGenerator::determineDirection(std::shared_ptr<Cell> cellFrom, std::shared_ptr<Cell> cellTo)
{
    if(cellFrom->getX() < cellTo->getX()) return Direction::RIGHT;
    else if(cellFrom->getX() > cellTo->getX()) return Direction::LEFT;
    else if(cellFrom->getY() < cellTo->getY()) return Direction::DOWN;
    else return Direction::UP;
}
