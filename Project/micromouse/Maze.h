#ifndef MAZE_H
#define MAZE_H
#include <vector>
#include <memory>
#include <QJsonObject>
#include "Cell.h"

class Maze
{
public:
    Maze(int size);
    Maze(const QJsonObject &obj);
    int getSize() const;
    void setRealCellSize(double size);
    double getRealCellSize();
    std::shared_ptr<Cell> getCell(int x, int y);
    std::shared_ptr<Cell> getCell(int id);
    std::shared_ptr<Cell> getNeighbourCell(std::shared_ptr<Cell> cell, Direction direction);
    std::shared_ptr<QJsonObject> toJson() const;
private:
    std::vector<std::vector<std::shared_ptr<Cell>>> cells;
    std::vector<std::vector<std::shared_ptr<Cell>>> generateEmptyMaze(int size);
    double cellSize = 0;
};

#endif // MAZE_H
