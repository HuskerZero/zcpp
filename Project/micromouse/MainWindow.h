#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <ui_mainwindow.h>
#include "Maze.h"
#include "Micromouse.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void onStartSignal();
    void onStopSignal(bool);
    void onDrawMaze(std::shared_ptr<Maze> maze);
    void onSetMouse(Micromouse *mouse);
    void onLCDTimeUpdate(int seconds);
    void onMazeComboUpdate(std::vector<std::string>);
    void onMouseComboUpdate(std::vector<std::string>);

private slots:
    void on_generateMazePushButton_clicked();
    void on_setMousePushButton_clicked();
    void on_startStopPushButton_clicked();
    void on_actionReset_triggered();

    void on_actionSave_maze_to_file_triggered();

    void on_actionLoad_maze_from_file_triggered();

signals:
    void signalStart();
    void signalStop(bool);
    void signalReset();
    void signalGenerateMaze(int size, std::string algorithm);
    void signalGenerateMicromouse(int speed, int turnSpeed, std::string algorithm);
    void signalSaveMaze();
    void signalLoadMaze();

private:
    void reset();
    QGraphicsScene scene;
    QGraphicsItem * drawCell(std::shared_ptr<Cell> cell, double x, double y, double cellSize);
    QTimer * simTimer;
};
#endif // MAINWINDOW_H


