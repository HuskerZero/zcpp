#include "WallFollowerBrain.h"

WallFollowerBrain::WallFollowerBrain()
{

}


std::pair<Action, Direction> WallFollowerBrain::decideNextMove(std::shared_ptr<Cell> cell, Direction currentDirection, Action currentAction)
{
    Direction lastDirection = currentDirection;

    if(cell->getConnections().size() > 1)
    {
        Direction oppositeDir = oppositeDirection(currentDirection);
        Direction newDirection = oppositeDir;
        while(newDirection == oppositeDir)
        {
            newDirection = getMostRightPossibleDirection(cell, currentDirection, oppositeDir);
        }
        currentDirection = newDirection;
    }
    else
    {
        currentDirection = cell->getConnections()[0];
    }

    if(lastDirection == currentDirection)
    {
        currentAction = Action::MOVING;
    }
    else
    {
        currentAction = Action::TURNING;
    }
    return {currentAction, currentDirection};
}

Direction WallFollowerBrain::getMostRightPossibleDirection(std::shared_ptr<Cell> cell, Direction currentDirection, Direction fromDirection)
{
    bool directionFound = false;

    //check if there is a way to the right
    Direction nextDir = getDirectionToTheRight(currentDirection);
    if(!cell->hasConnection(nextDir) && cell->hasConnection(currentDirection))
    {
        return currentDirection;
    }

    while(true)
    {
        nextDir = getDirectionToTheRight(currentDirection);
        if(cell->hasConnection(nextDir) && nextDir != fromDirection) return nextDir;
        currentDirection = nextDir;
    }
}

Direction WallFollowerBrain::getDirectionToTheRight(Direction currentDirection)
{
    switch(currentDirection)
    {
    case Direction::UP: return Direction::RIGHT;
    case Direction::DOWN: return Direction::LEFT;
    case Direction::LEFT: return Direction::UP;
    case Direction::RIGHT: return Direction::DOWN;
    }
}
