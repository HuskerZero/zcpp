#ifndef MICROMOUSE_H
#define MICROMOUSE_H

#include <QGraphicsItem>
#include <QObject>
#include <map>
#include "Maze.h"
#include "Direction.h"
#include "BruteForceBrain.h"

class Micromouse : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    Micromouse(double size, int speed, int turnSpeed, std::shared_ptr<Maze> maze, std::shared_ptr<IMouseBrain> brain);

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;

protected:
    void advance(int step) override;

private:
    const std::map<Direction, int> turnAngles;
    QColor color;
    double mouseSize = 0;
    double positionX = 0;
    double positionY = 0;
    std::shared_ptr<Maze> maze;
    int angleSpeed = 1;
    int forwardSpeed = 1;
    std::shared_ptr<IMouseBrain> mouseBrain;
    std::pair<int, int> lastDecisionPoint{100,100};
    Action currentAction = Action::MOVING;
    Direction lastDirection = Direction::RIGHT;
    Direction direction = Direction::RIGHT;
    double rotationAngle = 90;
    void turn();
    bool turnByStep();
    void move(Direction dir);
    bool moveByStep(Direction dir);
    bool clockwiseTurn();
    bool isWallInFront();
    void decideNextMove();
    bool isInCellCenter();
    bool isCrossroads();
    bool isStopRequired();
    bool isOnLastDecisionPoint();
    std::pair<int, int> positionToCellCoordinates();

signals:
    void signalFinishReached();
};

#endif // MICROMOUSE_H
