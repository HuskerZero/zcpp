#include "MouseCreator.h"
#include "BruteForceBrain.h"
#include "WallFollowerBrain.h"

MouseCreator::MouseCreator()
{

}

std::vector<std::string> MouseCreator::getAvailableAlgorithms()
{
    return std::vector<std::string> {"RandomBruteForce",
                                     "WallFollower"};
}

std::shared_ptr<IMouseBrain> MouseCreator::createMouse(std::string name)
{
    if(name == "RandomBruteForce") return std::make_shared<BruteForceBrain>();
    else if(name == "WallFollower") return std::make_shared<WallFollowerBrain>();
    else return nullptr;
}
