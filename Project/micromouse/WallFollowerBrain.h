#ifndef WALLFOLLOWERBRAIN_H
#define WALLFOLLOWERBRAIN_H

#include "IMouseBrain.h"

class WallFollowerBrain : public IMouseBrain
{
public:
    WallFollowerBrain();

    // IMouseBrain interface
public:
    virtual std::pair<Action, Direction> decideNextMove(std::shared_ptr<Cell> cell, Direction currentDirection, Action currentAction) override;
private:
    Direction getMostRightPossibleDirection(std::shared_ptr<Cell> cell, Direction currentDirection, Direction fromDirection);
    Direction getDirectionToTheRight(Direction currentDirection);
};

#endif // WALLFOLLOWERBRAIN_H
