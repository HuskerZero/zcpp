#include "RecursiveBacktrackerGenerator.h"
#include "RandomGenerator.h"
#include <algorithm>

RecursiveBacktrackerGenerator::RecursiveBacktrackerGenerator()
{

}


std::shared_ptr<Maze> RecursiveBacktrackerGenerator::generateMaze(int size)
{
    auto maze = std::make_shared<Maze>(size);
    int noElements = size * size;
    visited.reserve(noElements);

    RandomGenerator rand;
    auto cell = maze->getCell(0, 0);

    while (visited.size() < noElements)
    {
        cell->setVisited(true);
        addToVisited(cell->getId());

        auto unvisited = getUnvisitedDirections(cell, maze);

        if (unvisited.size() > 0)
        {
            path.push(cell->getId());
            auto direction = rand.generateRandom(unvisited);
            auto nextCell = maze->getNeighbourCell(cell, direction);
            createConnection(cell, nextCell, direction);
            cell = nextCell;
        }
        else
        {
            cell = maze->getCell(path.top());
            path.pop();
        }
    }
    return maze;
}

std::vector<Direction> RecursiveBacktrackerGenerator::getUnvisitedDirections(std::shared_ptr<Cell> cell, std::shared_ptr<Maze> maze)
{
    std::vector<Direction> directions;

    auto upCell = maze->getNeighbourCell(cell, Direction::UP);
    auto downCell = maze->getNeighbourCell(cell, Direction::DOWN);
    auto leftCell = maze->getNeighbourCell(cell, Direction::LEFT);
    auto rightCell = maze->getNeighbourCell(cell, Direction::RIGHT);

    if (upCell && !upCell->isVisited()) directions.push_back(Direction::UP);
    if (downCell && !downCell->isVisited()) directions.push_back(Direction::DOWN);
    if (leftCell && !leftCell->isVisited()) directions.push_back(Direction::LEFT);
    if (rightCell && !rightCell->isVisited()) directions.push_back(Direction::RIGHT);

    return directions;
}

void RecursiveBacktrackerGenerator::addToVisited(int id)
{
    auto foundId = std::find(visited.begin(), visited.end(), id);
    if (foundId == visited.end())
    {
        visited.push_back(id);
    }
}

void RecursiveBacktrackerGenerator::createConnection(std::shared_ptr<Cell> cellFrom, std::shared_ptr<Cell> cellTo, Direction direction)
{
    cellFrom->addConnection(direction);
    switch(direction)
    {
    case Direction::UP: cellTo->addConnection(Direction::DOWN); break;
    case Direction::DOWN: cellTo->addConnection(Direction::UP); break;
    case Direction::LEFT: cellTo->addConnection(Direction::RIGHT); break;
    case Direction::RIGHT: cellTo->addConnection(Direction::LEFT); break;
    }
}
