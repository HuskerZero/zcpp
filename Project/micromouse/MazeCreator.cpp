#include "MazeCreator.h"
#include "RecursiveBacktrackerGenerator.h"
#include "PrimGenerator.h"

MazeCreator::MazeCreator()
{

}

std::shared_ptr<Maze> MazeCreator::generateMaze(int size, std::string algorithm)
{
    return createGenerator(algorithm)->generateMaze(size);
}

std::vector<std::string> MazeCreator::getAvailableAlgorithms()
{
    return std::vector<std::string> {"RecursiveBacktracker",
                                     "Prim"};
}

std::shared_ptr<IMazeGenerator> MazeCreator::createGenerator(std::string name)
{
    if(name == "RecursiveBacktracker") return std::make_shared<RecursiveBacktrackerGenerator>();
    else if(name == "Prim") return std::make_shared<PrimGenerator>();
    else return nullptr;
}
