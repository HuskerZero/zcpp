#include "SimulationController.h"
#include <iostream>
#include <QTimer>
#include <QFile>
#include <QJsonDocument>
#include <QFileDialog>
#include "FileManager.h"

SimulationController::SimulationController(std::shared_ptr<MainWindow> window, QObject *parent) : QObject(parent), window(window)
{
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &SimulationController::updateElapsedTime);

    //connect ui to controller
    connect(window.get(), &MainWindow::signalGenerateMaze, this, &SimulationController::onGenerateMaze);
    connect(window.get(), &MainWindow::signalGenerateMicromouse, this, &SimulationController::onGenerateMicromouse);
    connect(window.get(), &MainWindow::signalStart, this, &SimulationController::onStartSignal);
    connect(window.get(), &MainWindow::signalStop, this, &SimulationController::onStopSignal);
    connect(window.get(), &MainWindow::signalReset, this, &SimulationController::onReset);
    connect(window.get(), &MainWindow::signalSaveMaze, this, &SimulationController::onSaveMaze);
    connect(window.get(), &MainWindow::signalLoadMaze, this, &SimulationController::onLoadMaze);

    //connect controller to ui
    connect(this, &SimulationController::signalStart, window.get(), &MainWindow::onStartSignal);
    connect(this, &SimulationController::signalStop, window.get(), &MainWindow::onStopSignal);
    connect(this, &SimulationController::signalAvailableMazeGenerationAlgorithms, window.get(), &MainWindow::onMazeComboUpdate);
    connect(this, &SimulationController::signalAvailableMicromouseAlgorithms, window.get(), &MainWindow::onMouseComboUpdate);
    connect(this, &SimulationController::signalCurrentTime, window.get(), &MainWindow::onLCDTimeUpdate);
    connect(this, &SimulationController::signalDrawMaze, window.get(), &MainWindow::onDrawMaze);
    connect(this, &SimulationController::signalSetMouse, window.get(), &MainWindow::onSetMouse);

    emit signalAvailableMazeGenerationAlgorithms(mazeCreator.getAvailableAlgorithms());
    emit signalAvailableMicromouseAlgorithms(mouseCreator.getAvailableAlgorithms());
}

void SimulationController::updateElapsedTime()
{
    endTime = std::chrono::steady_clock::now();
    elapsedTimeInMillis += std::chrono::duration_cast<std::chrono::milliseconds> (endTime - startTime).count();
    startTime = std::chrono::steady_clock::now();
    emit signalCurrentTime(elapsedTimeInMillis / 1000.0);
}

void SimulationController::onStartSignal()
{
    qDebug() << "Simulation started!\n";
    timer->start(100);
    startTime = std::chrono::steady_clock::now();
    emit signalStart();
}

void SimulationController::onStopSignal(bool reset)
{
    qDebug() << "Simulation stopped!\n";
    timer->stop();
    if(reset) onReset();
    updateElapsedTime();
    emit signalStop(reset);
}

void SimulationController::onGenerateMaze(int size, std::string algorithm)
{
    maze = mazeCreator.generateMaze(size, algorithm);
    emit signalDrawMaze(maze);
}

void SimulationController::onGenerateMicromouse(int speed, int turnSpeed, std::string algorithm)
{
    auto brain = mouseCreator.createMouse(algorithm);
    auto mouse = new Micromouse(maze->getRealCellSize() / 2., speed, turnSpeed, maze, brain);
    connect(mouse, &Micromouse::signalFinishReached, this, &SimulationController::onMouseReachedFinish);
    emit signalSetMouse(mouse);
}

void SimulationController::onReset()
{
    elapsedTimeInMillis = 0;
    emit signalAvailableMazeGenerationAlgorithms(mazeCreator.getAvailableAlgorithms());
}

void SimulationController::onSaveMaze()
{
    qDebug() << "Saving maze to file";
    QJsonDocument saveDoc(*maze->toJson().get());
    FileManager::saveFile(saveDoc);
}

void SimulationController::onLoadMaze()
{
    QJsonDocument loadDoc = FileManager::loadFile();
    maze = std::make_shared<Maze>(loadDoc.object());
    emit signalDrawMaze(maze);
}

void SimulationController::onMouseReachedFinish()
{
    onStopSignal(false);
}
