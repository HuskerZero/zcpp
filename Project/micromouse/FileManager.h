#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#include <QJsonDocument>

class FileManager
{
public:
    FileManager() = delete;
    static void saveFile(QJsonDocument &doc);
    static QJsonDocument loadFile();
};

#endif // FILEMANAGER_H
