#include "MainWindow.h"

#include <QApplication>
#include <iostream>
#include "SimulationController.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    std::shared_ptr<MainWindow> window = std::make_shared<MainWindow>();
    window->show();
    SimulationController controller(window);
    return a.exec();
}
