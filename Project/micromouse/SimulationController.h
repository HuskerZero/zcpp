#ifndef SIMULATIONCONTROLLER_H
#define SIMULATIONCONTROLLER_H

#include <chrono>
#include <QObject>
#include "MainWindow.h"
#include "Maze.h"
#include "Micromouse.h"
#include "MazeCreator.h"
#include "MouseCreator.h"

class SimulationController : public QObject
{
    Q_OBJECT
public:
    explicit SimulationController(std::shared_ptr<MainWindow> window, QObject *parent = nullptr);

private:
    std::shared_ptr<MainWindow> window;
    std::shared_ptr<Maze> maze;
    std::shared_ptr<Micromouse> mouse;
    MazeCreator mazeCreator;
    MouseCreator mouseCreator;
    std::chrono::steady_clock::time_point startTime;
    std::chrono::steady_clock::time_point endTime;
    long elapsedTimeInMillis = 0;
    QTimer *timer;
    void updateElapsedTime();

public slots:
    void onStartSignal();
    void onStopSignal(bool);
    void onGenerateMaze(int, std::string);
    void onGenerateMicromouse(int, int, std::string);
    void onReset();
    void onSaveMaze();
    void onLoadMaze();
    void onMouseReachedFinish();
signals:
    void signalStart();
    void signalStop(bool);
    void signalCurrentTime(int);
    void signalAvailableMazeGenerationAlgorithms(std::vector<std::string>);
    void signalAvailableMicromouseAlgorithms(std::vector<std::string>);
    void signalDrawMaze(std::shared_ptr<Maze>);
    void signalSetMouse(Micromouse*);
};

#endif // SIMULATIONCONTROLLER_H
