#ifndef RECURSIVEBACKTRACKERGENERATOR_H
#define RECURSIVEBACKTRACKERGENERATOR_H

#include <vector>
#include <stack>
#include "IMazeGenerator.h"

class RecursiveBacktrackerGenerator : public IMazeGenerator
{
public:
    RecursiveBacktrackerGenerator();

    // IMazeGenerator interface
public:
    virtual std::shared_ptr<Maze> generateMaze(int size) override;
private:
    std::vector<Direction> getUnvisitedDirections(std::shared_ptr<Cell> cell, std::shared_ptr<Maze> maze);
    void addToVisited(int id);
    void createConnection(std::shared_ptr<Cell> cellFrom, std::shared_ptr<Cell> cellTo, Direction direction);
    std::vector<int> visited;
    std::stack<int> path;

};

#endif // RECURSIVEBACKTRACKERGENERATOR_H
