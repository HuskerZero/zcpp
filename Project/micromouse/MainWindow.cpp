#include "MainWindow.h"
#include <QGraphicsPolygonItem>
#include "Micromouse.h"
#include <cmath>
#include <QDebug>
#include <QVector>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setupUi(this);
    simTimer = new QTimer(this);
    connect(simTimer, &QTimer::timeout, &scene, &QGraphicsScene::advance);
    graphicsView->setScene(&scene);
}

MainWindow::~MainWindow()
{
    destroy();
}

void MainWindow::onStartSignal()
{
    startStopPushButton->setText("Stop simulation!");
    generateMazePushButton->setEnabled(false);
    setMousePushButton->setEnabled(false);
    simTimer->start(1000 / 33);
}

void MainWindow::onStopSignal(bool reset)
{
    startStopPushButton->setText("Start simulation!");
    generateMazePushButton->setEnabled(true);
    setMousePushButton->setEnabled(true);
    simTimer->stop();
    if(reset) this->reset();
}

void MainWindow::onSetMouse(Micromouse *mouse)
{
    scene.addItem(mouse);
    graphicsView->update();
    startStopPushButton->setEnabled(true);
}

void MainWindow::onLCDTimeUpdate(int seconds)
{
    lcdNumber->display(seconds);
}

void MainWindow::onMazeComboUpdate(std::vector<std::string> values)
{
    mazeGenComboBox->clear();
    QStringList list;
    for(const std::string &str : values) { list.append(QString::fromStdString(str)); }
    mazeGenComboBox->addItems(list);
}

void MainWindow::onMouseComboUpdate(std::vector<std::string> values)
{
    mouseAlgComboBox->clear();
    QStringList list;
    for(const std::string &str : values) { list.append(QString::fromStdString(str)); }
    mouseAlgComboBox->addItems(list);
}

void MainWindow::reset()
{
    scene.clear();
    lcdNumber->display(0);
    setMousePushButton->setEnabled(false);
    startStopPushButton->setEnabled(false);
}

void MainWindow::onDrawMaze(std::shared_ptr<Maze> maze)
{
    scene.clear();
    int maxX = this->graphicsView->width();
    int maxY = this->graphicsView->height();
    scene.setSceneRect(0, 0, maxX, maxY);
    scene.setItemIndexMethod(QGraphicsScene::NoIndex);
    int size = maze->getSize();
    double cellSize = (double(maxX) / double(size));

    qDebug() << "<< MAZE PARAMETERS >>\n" << "Maze width: " << maxX << " | Maze height: " << maxY
              << " | Cell size: " << cellSize << " | Maze size: " << size << "x" << size << "\n";
    maze->setRealCellSize(cellSize);

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            auto cell = maze->getCell(i,j);
            scene.addItem(drawCell(cell, double(i) * cellSize, double(j) * cellSize, cellSize));
        }
    }
    this->graphicsView->update();

    setMousePushButton->setEnabled(true);
}

QGraphicsItem * MainWindow::drawCell(std::shared_ptr<Cell> cell, double x, double y, double cellSize)
{
    qDebug() << "Drawing cell " << cell->getId() << ": x = " << x << " | y = " << y << "\n";

    QGraphicsItemGroup * group = new QGraphicsItemGroup();

    if (!cell->hasConnection(Direction::UP))
    {
        group->addToGroup(new QGraphicsLineItem(x, y, x + cellSize, y, group));
    }
    if (!cell->hasConnection(Direction::DOWN))
    {
        group->addToGroup(new QGraphicsLineItem(x, y + cellSize, x + cellSize, y + cellSize, group));
    }
    if (!cell->hasConnection(Direction::LEFT))
    {
        group->addToGroup(new QGraphicsLineItem(x, y, x, y + cellSize, group));
    }
    if (!cell->hasConnection(Direction::RIGHT))
    {
        group->addToGroup(new QGraphicsLineItem(x + cellSize, y, x + cellSize, y + cellSize, group));
    }

    if(cell->getCellType() == Cell::START)
    {
        auto text = new QGraphicsTextItem("START");
        text->setPos(x, y);
        group->addToGroup(text);
    }
    else if(cell->getCellType() == Cell::FINISH)
    {
        auto text = new QGraphicsTextItem("FINISH");
        text->setPos(x, y);
        group->addToGroup(text);
    }

    return group;
}


void MainWindow::on_generateMazePushButton_clicked()
{
    emit signalGenerateMaze(mazeSizeSpinBox->value(), mazeGenComboBox->currentText().toStdString());
}

void MainWindow::on_setMousePushButton_clicked()
{
    emit signalGenerateMicromouse(int(speedMouseDoubleSpinBox->value()), int(speedTurnMouseDoubleSpinBox->value()), mouseAlgComboBox->currentText().toStdString());
}

void MainWindow::on_startStopPushButton_clicked()
{
    if(simTimer && simTimer->isActive())
    {
        emit signalStop(false);
    }
    else
    {
        emit signalStart();
    }
}

void MainWindow::on_actionReset_triggered()
{
    if(simTimer->isActive())
    {
        emit signalStop(true);
    }
    else
    {
        qDebug() << "Resetting UI\n";
        reset();
        qDebug() << "Sending reset request to backend\n";
        emit signalReset();
    }
}

void MainWindow::on_actionSave_maze_to_file_triggered()
{
    emit signalSaveMaze();
}

void MainWindow::on_actionLoad_maze_from_file_triggered()
{
    emit signalLoadMaze();
}
